<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 7 ]><html lang="sl" class="ie7"><![endif]-->
<!--[if gt IE 7]><!--><html lang="sl"><!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../../css/dessert.css" />
</head>
<style>
#header{ background:#FFF; height:29px; left:0; overflow:hidden; position:fixed; top:0; width:100%; z-index:1; }
    strong { float:left; color:#555; font-size:1.5em; margin:0 0 0 7px; text-shadow:#E8E8E8 1px 1px 3px; }
    div a { color:#777; float:right; font:bold 1.5em 'Courier New', Courier, monospace; margin:0 40px 0 0; outline:none; padding:2px 20px; text-decoration:none;
            -moz-border-radius-topleft: 6px; -moz-border-radius-topright: 6px; -moz-border-radius-bottomright: 0px; -moz-border-radius-bottomleft: 0px;
            -webkit-border-radius: 6px 6px 0px 0px;
            border-radius: 6px 6px 0px 0px; }
    div a:hover { background:#333; color:#A3CF00; }
    div a.active { background:#333; color:#FFF; }
pre { margin:29px 0 0; }
.ie7 pre { margin:14px 0 0; }
</style>
<body>
<div id="header">
    <strong>Pressclip.si</strong>
    <a href="/files/pressclip.si/js.php" rel="nofollow">JS</a>
    <a class="active" href="/files/pressclip.si/css.php" rel="nofollow">CSS</a>
    <a href="/files/pressclip.si/index.php" rel="nofollow">HTML</a>
</div>
<pre class="prettyprint lang-css">

<?php include('styles.css'); ?>

</pre>

<script src="../../js/plugins/codeprettify/prettify.js"></script>    <!-- http://code.google.com/p/google-code-prettify/ -->
<script src="../../js/plugins/codeprettify/lang-css.js"></script>
<script>prettyPrint();</script>
</body>
</html>	