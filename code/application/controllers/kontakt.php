<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontakt extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
        $this->load->library(array('form_validation', 'email'));
		$this->load->helper(array('form', 'url'));
	}
	function index()
	{ 
        $this->form_validation->set_rules('name', 'Ime', 'trim|required|max_length[20]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('subject', 'Zadeva', 'max_length[200]|xss_clean');
        $this->form_validation->set_rules('message', 'Sporočilo', 'trim|required|min_length[2]|max_length[9999]');

		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
        
		if ($this->form_validation->run())
		{
            $this->email->from(set_value('email'), set_value('name'));
            $this->email->to('info@codepsd.si');
    
			$this->email->subject("codepsd.si->KONTAKT | ".set_value('subject'));
			$this->email->message(set_value('message'));
            
            if($this->email->send())
            {
                $data['message'] = '<span class="success">Sporočilo je bilo uspešno poslano!</span>';
                $this->load->view('v_kontakt', $data);
            }
            else
            {
                //show_error($this->email->print_debugger());
                $data['message'] = '<span class="success error">"Prišlo je do napake! Sporočilo ni bilo poslano. Pošljite nam mail na <a href="mailto:info@codepsd.si">info@codepsd.si</a></span>';
                $this->load->view('v_kontakt', $data);
            }
        }

		else
		{
            $data['message'] = '';
            $this->load->view('v_kontakt', $data);
		}
	}
}