<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Pogosta vprašanja - Razrez PSD</title>
    <meta name="description" content="Imate kakšno vprašanje? Preberite odgovore na naša pogosto zastavljena vprašanja ali pa nas kontaktirajte." />
    <meta name="keywords" content="html5, css3, javascript, jquery, css sprites, razrez spletne predloge, razrez" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>

            <nav>
                <ul>    <!-- side navigation -->
                    <li class="active"><?php echo anchor('pogosta-vprasanja#technique','Tehnika'); ?></li>
                    <li><?php echo anchor('pogosta-vprasanja#procedure','Izvedba'); ?></li>
                    <li><?php echo anchor('pogosta-vprasanja#other-requests','Ostale vaše želje'); ?></li>
                    <li><?php echo anchor('pogosta-vprasanja#payment','Plačilo'); ?></li>
                    <li><?php echo anchor('pogosta-vprasanja#speedup','Pospešitev'); ?></li>
                    <li><?php echo anchor('pogosta-vprasanja#rights','Vaše pravice'); ?></li>
                </ul>    <!-- end navigation-->
            </nav>
        </aside>    <!-- end sidebar -->


        <div class="faq" id="content" >    <!-- content -->
            <article id="technique">
                <h1>Pogosta <span>vprašanja</span> in <span>odgovori</span></h1>

                <h3><span>Tehnična</span> vprašanja</h3>

                <h4>
                    DELATE RAZREZE SAMO Z <abbr title="Extensible Hyper Text Markup Language">XHTML1.0</abbr>
                    ALI TUDI ŽE S <abbr title="Hyper Text Markup Language Version 5">HTML5</abbr>?
                </h4>    
                <p>Delamo oboje. Odločitev je vaša.</p>

                <h4>
                    DELATE RAZREZE S <abbr title="Cascading Style Sheets Level 2 Revision 1">CSS 2.1</abbr>
                    ALI TUDI ŽE S <abbr title="Cascading Style Sheets Level 3">CSS3</abbr>?
                </h4>
                <p>Delamo oboje. Odločitev je prav tako vaša.</p>

                <h4>ALI LAHKO DELATE Z <abbr title="Joint Photographics Experts Group">JPG</abbr> VERZIJO MOJEGA DESIGNA?</h4>
                <p>
                    Lahko razrežemo vsako sliko (<abbr title="Joint Photographics Experts Group">JPG</abbr>,
                    <abbr title="Graphics Interchange Format">GIF</abbr>, <abbr title="Portable Network Graphics">PNG</abbr> itd.)
                    ali celo vašo obstoječo kodo v HTML. Najlaže in najhitreje pa delamo z lepo in smiselno urejenim <abbr title="Photoshop Document">PSD</abbr>-formatom.
                </p>

                <strong>S KAKŠNIM ORODJEM PRETVARJATE NAŠ DESIGN?</strong>
                <p>Naša koda je pisana ročno. Koda je zato čista, pregledna, komentirana in sproti optimizirana za najboljše delovanje.</p>

                <strong>KAKO NATANČNI STE?</strong>
                <p>
                    Videz designa v brskalniku bo do piksla natančno takšen, kot je na sliki.
                    To nam omogoča že zgoraj omenjeno ročno kodiranje, sprotno preverjanje in naša natančnost.
                </p>

                <h4>IMPLEMENTIRATE TUDI <abbr title="Skriptni Programski Jezik">JAVASCRIPT</abbr> IN <abbr title="JavaScript Knižnjica">JQUERY?</abbr></h4>
                <p>
                    Lahko implementiramo JavaScript za vas. Ne samo standarden JavaScript ampak tudi različne JavaScript knjižnice
                    (jQuery ...).
                </p>

                <strong>ŠE PODPIRATE INTERNET EXPLORER 6?</strong>
                <p>
                    Po privzetem je vsak design je razrezan in optimiziran, da deluje v Google Chrome, Mozilla Firefox 3.6+, Opera 11+, Safari 5+ in Internet Explorer 7+.
                    Internet Explorer 6 pa ne več. Google, YouTube, Wordpress in še mnogo drugih je že zdavnaj opustilo podporo zanj.
                    Od avgusta 2011 Google ne podpira niti brskalnika Internet Explorer 7. Mi ga da pa še vedno.
                </p>

                <h4>SEMANTIKA IN <abbr title="Search Engine Optimization">SEO</abbr>?</h4>
                <p>
                    Naslovne tag-e (H1, H2 ...) uporabljamo na pravilnih mestih. Pravilno zapiramo vse tage. Ne uporabljamo praznih div-ov za clearanje floatov.
                    ID in class uporabljamo minimalno, da je nadaljna implementacija v <abbr title="Content Managemnet System">CMS</abbr> lažja.
                    ID in class imajo smiselna imena. Uporabljamo &lt;blockquote&gt;, &lt;abbr&gt;, &lt;address&gt; itd.
                </p>

                <strong>OPTIMIZACIJA ZA HITROST NALAGANJA?</strong>
                <p>
                    Vaša stran mora biti čim lahkotnejša, da je čas nalaganja minimalen. Mi optimiziramo vaše slike tako, da jim stisnemo velikost.
                    Minimiziramo lahko tudi datoteke CSS in JavaScript in zmanjšamo število zahtevkov <abbr title="Hypertext Transfer Protocol">HTTP</abbr>.
                    Slike vam lahko optimiziramo tudi s tehniko <a href="http://www.w3schools.com/css/css_image_sprites.asp">CSS sprites</a>.
                </p>
            </article>

            <article id="procedure">
                <h3>Postopek <span>izvedbe</span></h3>

                <strong>KOLIKO ČASA PO NAVADI POTREBUJETE ZA IZVEDBO?</strong>
                <p>
                    Čas dostave je odvisen od obsega vašega naročila. Navadno pa je to v zelo grobi oceni okrog 7 ur za prvo stran
                    (X)HTML. Ostale podstrani, če so podobne tej pa nato veliko manj (že od 1 ure dalje).
                </p>

                <strong>KAKO DOSTAVITE SVOJE IZDELKE?</strong>    
                <p>
                    Končane izdelke shranimo kot skin, temo, predlogo ali CMS.
                    Te datoteke so potem navadno zazipane in poslane na vaš mail.
                </p>
            </article>

            <article id="other-requests">
                <h3>Ostale <span>vaše želje</span></h3>

                <strong>BI PREUREDILI/PRENOVILI NAŠO ŽE OBSTOJEČO STRAN?</strong>
                <p>
                    Lahko! Zazipajte datoteke in nam jih pošljite prek našega  <?php echo anchor('naroci','naročilnega'); ?> obrazca ali
                    na naš elektronski naslov <a href="mailto:info@codepsd.si">info@codepsd.si</a>. Lahko pa nam samo pošljete
                    uporabniško ime in geslo za dostop do vašega strežnika pa bomo vse storili mi.
                </p>

                <strong>RAZREŽETE PREDLOGE TUDI ZA EMAIL?</strong>
                <p>Da. Email nato testiramo na: GMail, Hotmail, Microsoft Outlook, Thunderbird in še na kakšnem, če želite.</p>

                <strong>RADI BI INTEGRIRALI SVOJ DESIGN V KAKŠEN <abbr title="Content Managemnet System">CMS</abbr>. NAM LAHKO POMAGATE?</strong>
                <p>
                    Mi ponujamo tudi integracijo v <a href="http://drupal.org/">Drupal</a> CMS.
                    Pomagali pa vam bomo tudi najti zunanje izvajalce za katerega od ostalih ostalih CMS -jev, če si to želite. 
                </p>

                <strong>DELATE TUDI POPRAVKE ALI SPREMEMBE?</strong>
                <p>
                    Če je potrebno, da. Delamo, dokler vi niste zadovoljni. Če je popravek naša napaka, dela ne zaračunamo.
                    V ostalih primerih se čas, potreben za izvedbo spremembe, računa po normalni urni postavki.
                </p>

                <strong>ALI NAM LAHKO SVETUJETE KAKŠNEGA DOBREGA OBLIKOVALCA?</strong>
                <p>Seveda. Najverjetneje ravno mi poznamo največ dizajnerjev, ker največ delamo z njimi in imamo z njimi največ stikov.</p>

                <strong>BI LAHKO POKAZALI NAŠO STRAN V VAŠEM PORTFOLIU?</strong>
                <p>Z veseljem bomo pokazali vašo stran v našem portfoliu. In ne, ne bomo pokazali vaše strani, če vi tega ne želite.</p>
            </article>
            
            <article id="payment">
                <h3><span>Plačilo</span></h3>

                <strong>KAKO RAČUNATE VAŠE STORITVE?</strong>
                <p>
                    Naše storitve računamo po urah, potrebnih za opravljeno delo. V zelo grobi oceni je to okrog 7 ur za prvo stran.
                    Ostale podstrani, če so podobne tej, pa nato veliko manj (že od 1 ure dalje).
                </p>

                <strong>KOLIKO RAČUNATE?</strong>    
                <p>
                    Računamo od 16 € na uro, odvisno kako hitro potrebujete datoteke. Če jih potrebujete v parih dneh, je urna postavka 16 €.
                    Če imamo rok za začet z delom 10 dni ali več, je naša urna postavka 14 €. Če pa jih potrebujete "Takoj!", kar pomeni da moramo mi delati ves dan ali celo čez vikend,
                    pa je ta postavka 20 €+ na uro.
                </p>

                <strong>KOLIKO NAS BI STAL RAZREZ?</strong>    
                <p>
                    Tega vam ne moremo napovedati, ne da bi točno vedeli kaj potrebujte.
                    Povemo pa vam lahko, ko nam vi poveste, kako vam lahko pomagamo oziroma, ko nam pošljete slikovne datoteke.
                    Kontaktirajte nas prek <?php echo anchor('kontakt','kontaktnega obrazca'); ?> ali pa nam pošljite datoteke prek <?php echo anchor('kontakt','obrazca naročilo'); ?>.
                    Po natančnem in brezplačnem pregledu vam bomo takoj odgovorili in sporočili oceno. 
                </p>

                <strong>NEKATERI RAČUNAJO FIKSNO KVOTO PO POSAMEZNIH STRANEH. ZAKAJ NE TUDI VI?</strong>
                <p>
                    Strani so zelo različne, zato menimo, da je taka ocena nemogoča in nerealna.
                    Ena stran je lahko končana v 2 urah, spet za drugo pa lahko porabimo ves dan.
                    Če bi podali fiksno ceno, ta ne more biti niti malo realna in bi bil v tem primeru eden od nas ogoljufan.
                    Ali bi vi preplačali ali pa bi mi morali delati več, kot smo plačani. Upamo si trditi, da si tudi vi tega ne želite.
                    Mi verjamemo, podpiramo in želimo pošten in dolgotrajen odnos!
                </p>

                <strong id="quote">KAKO JE S PLAČILOM IN KDAJ PLAČAMO?</strong>
                <p>
                    Za razrez in manjše storitve potrebujemo plačilo v naprej. Pri večjih storitvah in implementaciji v CMS pa poslujemo tako,
                    da stranka plača vsaj polovico zneska v naprej. Po tem pa mi takoj začnemo z delom.
                </p>

                <strong>NAM LAHKO VRNETE DENAR?</strong>
                <p>
                    Seveda! Garantiramo vam, da vam vrnemo celoten znesek, če boste zahtevali preklic naše storitve,
                    preden začnemo z delom, ali v primeru, če niste zadovoljni s končanim izdelkom.
                </p>

                <strong>PRIMER NAŠE OCENE ZA DELO, KI VAM JO POŠLJEMO:</strong>
				<table>
					<thead>
						<tr><th>Pozdravljeni.<br /><br />Za razrez vaših 5 predlog bi porabili:</th><th></th></tr>
					</thead>
					<tfoot>
						<tr><th>SKUPAJ</th><th></th></tr>
						<tr><td>= 14 ur</td><td>224 €</td></tr>
					</tfoot>
					<tbody>
						<tr><th>psd_01.psd</th><th></th></tr>
						<tr><td>* header</td><td>1,5 ure</td></tr>
						<tr><td>* vsebinski del</td><td>2,5 ure</td></tr>
						<tr><td>* footer</td><td>2 uri</td></tr>

						<tr><th>psd_02.psd</th><th></th></tr>
						<tr><td>* CSS drop down meni</td><td>2,5 ure</td></tr>
						<tr><td>* vsebniski del</td><td>1 uro</td></tr>
						
						<tr><th>psd_03.psd</th><th></th></tr>
						<tr><td>* header</td><td>0,5 ure</td></tr>
						<tr><td>* vsebinski del</td><td>1 uro</td></tr>
						
						<tr><th>psd_04.psd</th><th></th></tr>
						<tr><td>* prijava</td><td>1 uro</td></tr>
						
						<tr><th>psd_05.psd</th><th></th></tr>
						<tr><td>* vsebinski del</td><td>1 uro</td></tr>
						
						<tr><th>Ostalo</th><th></th></tr>
						<tr><td>* testiranje in popravki v drugih brskalnikih</td><td>1 uro</td></tr>
					</tbody>
				</table>
            </article>

            <article id="speedup">
                <h3><span>Pospešimo</span> izvedbo</span></h3>
                
                <h4>POŠLJITE NAM DIZAJN V PHOTOSHOPOVEM <abbr title="Photoshop Document">PSD</abbr> FORMATU</h4>    
                <p>Psd naj bo lepo urejen, layerji smiselno imenovani, združeni v skupine ...</p>
                
                <strong>DIZAJNER LAHKO UPORBLJA ENEGA OD NAŠTETIH FONTOV</strong>
                <p>
                    Designer lahko uporablja vse večjo bazo <a href="http://www.google.com/webfonts" target="_blank">Google webfont-ov</a>
                    ali pa fonte iz ogromne baze, ki jo najde na <a href="http://www.fontsquirrel.com/" target="_blank">fontsquirell.com</a>. 
                    Če bo tekst na vaši strani tudi odebeljen ali napisan ležeče naj designer preveri, če izbrani font to tudi podpira.
                </p>
                
                <strong>POŠLJITE NAM FONTE V .EOT IN <abbr title="TrueType Font">.TTF</abbr> zapisu</strong>
                <p>
                    Če designer ne uporablja zgornjih rešitev, nam priskrbite fonte v .eot in .ttf zapisih.
                    Eos zapis je za Internet Explorer, medtem ko potrebujemo .ttf zapis za vse ostale brskalnike.
                    Če bo tekst na vaši strani odebeljen ali napisan ležeče, naj designer preveri, ali izbrani font to tudi podpira.
                </p>
                
                <strong>NAPIŠITE ČIM BOLJ PODROBNA NAVODILA</strong>
                <p>
                    Da se izognemo dodatnim popravkom so podrobna navodila zelo priporočljiva. 
                    Slaba navodila pogosto pomenijo dvojno delo. Če bo to delo naše, se zahteva tudi plačilo.
                    Zaželena so navodila recimo kot: "Tisti seznam jedi želim v obliki floatanih &lt;div&gt; in ne kot &lt;ul&gt; &lt;li&gt;, kot je to v navadi.
                    Naslov nad njimi pa naj bo &lt;h2&gt;." Še bolje pa je, če naredite screenshot in to označite na samem screenshotu.
                </p>
                
                <strong>NAŠE DELO NAS OBOJE STANE</strong>
                <p>
                    Če privarčujete naš čas, s tem posledično privarčujete tudi svoj denar.
                    Upoštevajte zgornje in privarčujte kar nekaj ur, torej kar nekaj denarja.
                </p>
            </article>

            <article id="rights">
                <h3>Vaše <span>pravice</span> in <span>zasebnost</span></h3>

                <strong>SPOŠTUJETE ZASEBNOST?</strong>
                <p>
                    Absolutno. Vaša zasebnost je zelo pomembna. Spoštujemo vaše avtorske pravice. Spoštujemo vašo zasebnost in odnos,
                    ki ste ga zgradili s svojimi strankami. Vašo originalnost bomo uporabili samo zato, da vam izdelamo kakovostno HTML ogrodje in CSS.
                    Vaše informacije vzamemo resno in zaupno, zato jih nikoli ne bomo posredovali kakršni koli zunanji strani.
                    Spoštujemo in cenimo vašo zasebnost, zato nikoli ne bi stopili neposredno v stik z vašimi strankami za kakršen koli namen.
                    Imate našo besedo! Lahko vam jo damo tudi pisno.
                </p>

                <strong>NAŠA OBLJUBA ZASEBNOSTI</strong>
                <p>
                    Spodaj je kratek dogovor oz. naša obljuba, da bo vaša intelektualna lastnina varna:
                    <br /><br />
                    <em>
                        Pri CodePsd.si spoštujemo vaše profesionalno umetniško delo, zato ga bomo uporabili samo za to, da vam pomagamo pri rešitvi vaših ciljev.
                        Vaše avtorske pravice bodo ostale vaše. Pri CodePsd.si vaše umetnine pod nobenim pogojem ne bomo uporabili na katerem koli drugem mestu, jo prodali ali komu dali.
                        CodePsd.si vam zagotavlja, da bodo vse informacije, ki jih delite z nami, vključno z grafičnimi, kontaktnimi podatki in strankami, ostale zaupne.
                        Pri CodePsd.si ne bomo nikoli kontaktirali vaših naročnikov ali naročnikov vaših naročnikov. Spoštujemo vaše podjetje.
                    </em>
                </p>

                <strong>
                    MI SMO SPLETNA AGENCIJA IN NE ŽELIMO RAZKRITI NAŠEGA SODELOVANJA Z VAMI. NE ŽELIMO NITI, DA BI DELO, OPRAVLJENO ZA NAS, UPORABILI V SVOJEM PORTFOLIU.
                    ŠE BOLJ POMEMBNO PA JE, DA NE ŽELIMO, DA BI VI NEPOSREDNO KONTAKTIRALI NAŠE STRANKE.
                </strong>
                <p>
                    Ne skrbite! Delamo z veliko agencijami, zato vemo, kako se stvarem streže. 
                    Tudi z vami lahko podpišemo pogodbo o nerazkritju informacij <abbr title="Non Disclosure Agreement">(NDA)</abbr>,
                    ker cenimo pošten in dolgotrajen odnos z našimi strankami.
                </p>

                <strong>KAJ PA LASTNIŠTVO IN PRAVICE?</strong>
                <p>
                    Ko je naše delo končano, postanete vi lastnik celotnega izdelka. To pa vključuje HTML, CSS, JavaScript, slike ...
                    Vse pravice so torej vaše, zato lahko z izdelkom delate, kar želite.
                </p>

                <strong>BOSTE PUSTILI KAKŠEN SVOJ PEČAT V KODI?</strong>
                <p>Ne! Vse pravice in avtorstvo v končanem izdelku pripada vam.</p>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>