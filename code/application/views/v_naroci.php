<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>Naročite našo storitev - CodePsd - Razrez PSD</title>
    <meta name="description" content="Naročite svoj razrez psd preko obrazca na naši strani ali pa nas pokličite na 070 758 408." />
    <meta name="keywords" content="xhtml, html5, css2.1, css3" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article id="contact">
                <h1>Naroči</h1>
                
                <strong>
                    Že od 16 € na uro dalje. Več o cenah in plačilu si preberite <?php echo anchor('pogosta-vprasanja#payment','tukaj'); ?>.
                </strong>

                <?php echo form_open_multipart('naroci', array('id' => 'upload')); ?>
                
                    <section class="radio">
                        <?php echo $message;?>
        
                        <div>
                            <span>Kdaj potrebujete datoteke?</span>
                            <input checked="checked" id="days2" name="days" type="radio" value="12" <?php echo $this->form_validation->set_radio('days', 'V PAR DNEH'); ?>/>
                            <label for="days2">V PARIH DNEH (16€ /URO)</label>
                            <input id="days3" name="days" type="radio" value="15" <?php echo $this->form_validation->set_radio('days', 'TAKOJ!'); ?>/>
                            <label for="days3">TAKOJ! (20€ /URO)</label>
                        </div>

                        <div>
                            <span>Izberite Doctype:</span>
                            <input id="xhtmlt" name="html" type="radio" value="XHTML 1.0 TRANSITIONAL" <?php echo $this->form_validation->set_radio('html', 'XHTML 1.0 TRANSITIONAL'); ?>/>
                            <label for="xhtmlt">XHTML 1.0 TRANSITIONAL</label>
                            <input id="xhtmls" name="html" type="radio" value="XHTML 1.0 STRICT" <?php echo $this->form_validation->set_radio('html', 'XHTML 1.0 STRICT'); ?>/>
                            <label for="xhtmls">XHTML 1.0 STRICT</label>
                            <input id="html5" name="html" type="radio" value="HTML 5" <?php echo $this->form_validation->set_radio('html', 'HTML 5'); ?>/>
                            <label for="html5">HTML 5</label>
                        </div>
        
                        <div>
                            <span>Izberite CSS verzijo:</span>
                            <input id="css2" name="css" type="radio" value="CSS 2.1" <?php echo $this->form_validation->set_radio('css', 'CSS 2.1'); ?>/>
                            <label for="css2">CSS 2.1</label>
                            <input id="css3" name="css" type="radio" value="CSS 3" <?php echo $this->form_validation->set_radio('css', 'CSS 3'); ?>/>
                            <label for="css3">CSS 3</label>
                        </div>
                    </section>
                        
                    <dl class="price">
                    	<dt>Prva stran:</dt>
                    	<dd>Približno 7 ur</dd>
                    	<dt>Podstran:</dt>
                    	<dd>Približno 4 ure</dd>
                        <dt>Koliko strani?</dt>
                    	<dd><input class="pages" id="pages" type="text" maxlength="2" size="2" value="1"/></dd>
                    	<dt>Okvirna cena:</dt>
                    	<dd class="total"></dd>
                    </dl>
                    <?php echo form_error('email'); ?>

                    <span>E-pošta*</span>
                    <input type="text" id="email" name="email" maxlength="75" placeholder="samo@podjetnik.si" value="<?php echo set_value('email'); ?>" />
                    <?php echo form_error('file[]'); ?>

                    <span>Dodaj datoteke ... </span>
                    <label class="input-file" for="upload-btn">
                        <small>(Velikost je omejena na 50MB. Dovoljene so datoteke psd, png, jpg, bmp, gif, zip, rar in 7z)</small>
                        <input id="upload-btn" type="file" name="file[]" multiple="multiple" title="Z vsemi brskalniki razen Internet Explorer-jem lahko izberete in pošljete več datotek naenkrat. Lahko pa jih pošljete zazipane." />
                        <img class="upload-img" src="/img/ajax-loader.gif" alt="razrez psd" height="15" width="128" />
                    </label>

                    <ul id="fileList"></ul>
                    <?php echo form_error('message'); ?>

                    <span>Sporočilo*</span>
                    <?php echo form_textarea( array( 'id' => 'message', 'name' => 'message', 'rows' => '10', 'cols' => '106', 'placeholder' =>'To je moje sporočilo', 'value' => set_value('message') ) )?>
                    
                    <?php echo form_submit( 'submit', 'Pošlji'); ?>
                    
                    <p class="error">Prosimo izpolnite vsa polja</p>
                </form>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>