<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Portfolio - Razrez PSD</title>
    <meta name="description" content="Poglejte primere razrezov spletne predloge / Naš portfolio" />
    <meta name="keywords" content="razrez psd, html5, css3, javascript, jquery, css sprites, razrez spletne predloge" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main" class="portfolio">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>

            <nav>
                <ul>    <!-- navigation -->
                    <li class="active"><?php echo anchor('portfolio/#nr1','Tournament.si'); ?></li>
                    <li><?php echo anchor('portfolio/#nr2','Sugarcreative.dk'); ?></li>
                    <li><?php echo anchor('portfolio/#nr3','Pressclip.si'); ?></li>
                </ul>    <!-- end navigation-->
            </nav>
        </aside>    <!-- end sidebar -->


        <div id="content">    <!-- content -->
            <article id="nr1">
                <h1>Primeri naših <span>izdelkov</span></h1>
                <p>
                    Imamo veliko število zadovoljnih strank, za katere smo naredili <?php echo anchor('razrez-psd','razrez PSD'); ?>.
                    Na tem mestu vam bomo predstavili nekaj primerov naših končnih izdelkov, ki so jih prejele stranke.
                </p>

                <strong>Tournament.si</strong>
                <figure>
                    <a href="/img/portfolio/tournament.si/razrez_psd.jpg">
                        <img src="/img/portfolio/tournament.si/razrez_psd_thumb.jpg" alt="pressclip.si" height="285" width="450" />
                    </a>
                    <p>
                        Naredili smo razrez spletne predloge za <a href="http://www.tournament.si" target="_blank">Tournament.si</a>.
                        Na željo naročnika smo slike naredili z CSS sprites tehniko.
                    </p>
                </figure>
                <section>
                    <strong>Tehnologija:</strong>
                    <p>
                        <abbr title="Hyper Text Markup Language Version 5">HTML5</abbr>, <abbr title="Cascading Style Sheets Level 3">CSS 3</abbr>
                    </p>
                    <strong>Posebne zahteve:</strong>
                    <p>* CSS sprites</p>
                    <strong>Koda:</strong>
                    <div class="files">
                        <a href="/files/tournament.si/index.php" rel="nofollow">HTML</a>
                        <a href="/files/tournament.si/css.php" rel="nofollow">CSS</a>
                        <a href="/files/tournament.si/js.php" rel="nofollow">JS</a>
                    </div>
                    <strong>Potreben čas za projekt:</strong>
                    <p> - 9 ur</p>
                    <strong>Prenesi celotno stran:&nbsp; <a class="zip" href="/files/tournament.si/pack.zip">ZIP</a></strong>
                </section>
            </article>
            
            <article id="nr2">
                <strong>Sugarcreative.dk</strong>
                <figure>
                    <a href="/img/portfolio/sugarcreative.dk/psd2html.jpg">
                        <img src="/img/portfolio/sugarcreative.dk/psd2html_thumb.jpg" alt="pressclip.si" height="301" width="450" />
                    </a>
                    <p>
                        Naredili smo razrez spletne predloge za <a href="http://www.sugarcreative.dk" target="_blank">Sugarcreative.dk</a>.
                    </p>
                </figure>
                <section>
                    <strong>Tehnologija:</strong>
                    <p>
                        <abbr title="Hyper Text Markup Language Version 5">HTML5</abbr>, <abbr title="Cascading Style Sheets Level 3">CSS 3</abbr>
                    </p>
                    <strong>Posebne zahteve:</strong>
                    <p>* HTML5</p>
                    <strong>Koda:</strong>
                    <div class="files">
                        <a href="/files/sugarcreative.dk/index.php" rel="nofollow">HTML</a>
                        <a href="/files/sugarcreative.dk/css.php" rel="nofollow">CSS</a>
                        <a href="/files/sugarcreative.dk/js.php" rel="nofollow">JS</a>
                    </div>
                    <strong>Potreben čas za projekt:</strong>
                    <p> - 8 ur</p>
                    <strong>Prenesi celotno stran:&nbsp; <a class="zip" href="/files/sugarcreative.dk/pack.zip">ZIP</a></strong>
                </section>
            </article>

            <article id="nr3">
                <strong>Pressclip.si</strong>
                <figure>
                    <a href="/img/portfolio/pressclip.si/psd_html.jpg">
                        <img src="/img/portfolio/pressclip.si/psd_html_thumb.jpg" alt="pressclip.si" height="301" width="450" />
                    </a>
                    <p>
                        Naredili smo razrez spletne predloge za <a href="http://www.pressclip.si" target="_blank">Pressclip.si</a>.
                        Naredili smo jim tudi css drop meni in lightbox fotogalerijo z jQuery.
                    </p>
                </figure>
                <section>
                    <strong>Tehnologija:</strong>
                    <p>
                        <abbr title="Hyper Text Markup Language Version 5">HTML5</abbr>, <abbr title="Cascading Style Sheets Level 3">CSS 3</abbr>,
                        <abbr title="Skriptni Programski Jezik">JavaScript</abbr> ( <abbr title="JavaScript knjižnica">jQuery</abbr> )
                    </p>
                    <strong>Posebne zahteve:</strong>
                    <p>* CSS drop meni</p>
                    <p>* jQuery fotogalerija</p>
                    <strong>Koda:</strong>
                    <div class="files">
                        <a href="/files/pressclip.si/index.php" rel="nofollow">HTML</a>
                        <a href="/files/pressclip.si/css.php" rel="nofollow">CSS</a>
                        <a href="/files/pressclip.si/js.php" rel="nofollow">JS</a>
                    </div>
                    <strong>Potreben čas za projekt:</strong>
                    <p> - 20 ur</p>
                    <strong>Prenesi celotno stran:&nbsp; <a class="zip" href="/files/pressclip.si/pack.zip">ZIP</a></strong>
                </section>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>