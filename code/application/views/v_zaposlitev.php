<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Kdo smo? - Razrez PSD</title>
    <meta name="description" content="Kdo smo? Predstavitev naše ekipe." />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article class="hire">
                <h1><span>Zaposlitev</span></h1>
                <section>
                    <h4>Prijav za zaposlitev trenutno ne sprejemamo</h4>
                </section>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>