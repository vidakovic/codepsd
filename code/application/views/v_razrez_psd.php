<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Razrez PSD, front-end razrez spletnih predlog</title>
    <meta name="description" content="Razrez PSD (PSD2HTML) pomeni pretvorbo spletne predloge v front-end HTML in CSS kodo" />
    <meta name="keywords" content="razrez psd, psd v html, razrez spletnih predlog, razrez spletne predloge, psd2html, front-end, razrez designa, razrez dizajna" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article id="psd">
                <h1>Razrez <span>PSD</span> v <span>HTML</span></h1>
                <h3>Kaj pomeni razrez <span>PSD v HTML</span> oz. <span>PSD2HTML</span> in kako se ga naredi?</h3>
                <p>
                    <strong>Razrez PSD</strong> ali drugače <strong>PSD2HTML</strong> ali še drugače <strong>HTML/CSS razrez</strong> ali pa
                    <strong>PSD v HTML</strong> pomeni z drugimi besedami <strong>razrez spletne predloge</strong> oziroma <strong>razrez designa</strong>.
                    Spletno predlogo oziroma prednji (<strong>front-end</strong>) videz spletne strani največkrat naredi spletni oblikovalec (dizajner).
                    Končano predlogo nato shrani v obliki kakšne slikovne datoteke (<abbr title="Portable Network Graphics">PNG</abbr>, 
                    <abbr title="Joint Photographics Experts Group">JPG</abbr>, <abbr title="Graphics Interchange Format">GIF</abbr>,
                    <abbr title="Bitmap Graphics Format">BMP</abbr> ...) Največkrat pa je to Photoshopova <abbr title="Photoshop Document">PSD</abbr>-datoteka.
                    Iz teh slikovnih datotek oz. slik je nato treba izrezat posamezne dele (sličice) in stran programirati s 
                    <abbr title="Hyper Text Markup Language">HTML</abbr>, <abbr title="Cascading Style Sheets">CSS</abbr> in po potrebi še kodo
                    <abbr title="Skriptni Programski Jezik">JavaScript</abbr>. HTML-koda je bolj kot ogrodje spletne strani.
                    V CSS-kodi določimo barve besedila, barve ozadja, velikost črk, kje naj bo posamezni element(besedilo, slika ...) prikazan na strani,
                    kako visok naj bo, kako dolg ipd. JavaScript pa je bolj za to kaj naj se zgodi pri kliku na nek element, kaj ko gre samo miška čez,
                    v glavnem, kako naj bo stran bolj živa. Lahko pa se uporabi tudi razne JavaScript knjižnice, kot so recimo <a href="http://jquery.com/">jQuery</a>,
                    <a href="http://mootools.net/">mootools</a>, <a href="http://www.prototypejs.org/">prototype</a> ipd., ki poenostavijo JavaScript programiranje.
                </p>
                <h3>Kdo lahko dela <span>razrez PSD</span>?</h3>
                <p>
                    <strong>Razrez PSD</strong> lahko naredi vsak, ki pozna vsaj Photoshop, HTML, CSS in še mogoče JavaScript.
                    Za dober razrez pa je treba poznati še težave brskalnikov, treba je karseda poenostaviti kodo, jo pisati semantično,
                    jo sproti pisati za <abbr title="Search Engine Optimization">SEO</abbr>, jo komentirati, jo pravilno zapirati, jo napisati tako,
                    da bo naknadno delo (integracija v CMS) karseda enostavno, uporabiti pravi ukaz na pravem mestu, biti zelo dosleden in natančen,
                    narediti CSS sprites, zmanjšati število zahtevkov HTTP in nato še to vse nekajkrat testirati v vsaj 8 brskalnikih
                    na Windows operacijskem sistemu ter vsaj v Applovem Safari brskalniku na OS X.
                </p>
                <h3>Kaj pa potem ko je <span>razrez PSD</span> končan?</h3>
                <p>
                    Ko je razrez končan, se lahko te datoteke naloži na strežnik in stran je končana. Večje strani in strani, katerih vsebina se pogosto spreminja
                    pa se navadno naredi s kakšnim <abbr title="Content Managemnet System">CMS</abbr>. Ta uporabniku omogoča, da lahko brez
                    programerskega znanja kar na internetu ureja in dodaja vsebino na spletno stran v urejevalniku, ki je zelo podoben Microsoft Wordu.
                    Končan razrez se zato navadno integrira v kakšen CMS, kot sta recimo <a href="http://wordpress.org/">Wordpress</a> in
                    <a href="http://drupal.org/">Drupal</a>.
                </p>
                <h3>Kje mi spremljamo nove tehnologije, nove standarde in novice za <span>front-end</span>?</h3>
                <p>
                    Nekateri berejo novice na 24ur.com, nekateri časopis Slovenske novice, mi pa prebiramo <a href="http://www.smashingmagazine.com/">Smashing Magazine</a>,
                    <a href="http://net.tutsplus.com/">Nettuts</a>, <a href="http://css-tricks.com/">CSS Tricks</a> in mnoge tem podobne. Od tod dobimo ogromno novih
                    informacij in smo zato lahko v koraku z novimi tehnologijami in standardi.
                </p>
                <h3>Imate kakšen <span>privzeti template</span>, s katerim začnete delo, da ne začnete zmeraj s prazno datoteko?</h3>
                <p>
                    Da. Naredili smo svoj template za HTML, CSS in JavaScript, s katerim po navadi začnemo delati.
                    Narejen je po standardih <a href="http://html5boilerplate.com/">HTML5 boilerplate</a>.
                    Pogledate, uporabljate in celo spreminjate ga lahko po svoji volji. 
                    Prenesete pa si ga <a href="/files/codepsd_boilerplate.zip">TUKAJ</a>.
                </p>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>