<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>Kontaktirajte nas - CodePsd - Razrez PSD</title>
    <meta name="description" content="Kontaktirajte nas preko obrazca na naši strani ali pa nas pokličite na 070 758 408." />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article id="contact">
                <h1>Kontakt</h1>
                
                <strong>
                    Kontaktirate nas lahko prek spodnjega obrazca. Lahko nam pošljete mail na <a href="mailto:info@codepsd.si">info@codepsd.si</a>
                    ali pa nas pokličete na 070 414 987
                </strong>

                <?php echo form_open_multipart('kontakt'); ?>

                    <section><?php echo $message;?>
                    <?php echo form_error('name'); ?>

                        <section class="text">
                            <input type="text" name="name" maxlength="25" placeholder="IME*" title="IME*" value="<?php echo set_value('name'); ?>" />
                            <?php echo form_error('email'); ?>

                            <input type="text" name="email" maxlength="50" placeholder="E-POŠTA*" title="E-POŠTA*" value="<?php echo set_value('email'); ?>" />
                            <?php echo form_error('subject'); ?>

                            <input type="text" name="subject" maxlength="100" placeholder="ZADEVA" title="ZADEVA" value="<?php echo set_value('subject'); ?>" />
                        </section>
                        <?php echo form_error('html'); ?>

                        <section class="address">
                            <p>Rok Bahor</p>
                            <p>Dobliče 32</p>
                            <p>8340 Črnomelj, Slovenia</p>
                        </section>
                    </section>  

                    <section>
                        <?php echo form_error('message'); ?>   
                        <?php echo form_textarea( array( 'name' => 'message', 'rows' => '10', 'cols' => '106', 'value' => set_value('message'), 'placeholder' =>'SPOROČILO*', 'title'=>'SPOROČILO*' ) )?>
                            
                        <?php echo form_submit( 'submit', 'Pošlji'); ?>

                    </section>
                    
                </form>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>