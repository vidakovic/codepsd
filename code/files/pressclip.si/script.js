﻿$(document).ready(function() {
    //Assign the ColorBox event to elements
    $("a[rel='gallery']").colorbox({transition:"none", height:"80%"});
    $("a[class='forgot-pass']").colorbox({innerWidth:"535px", innerHeight:"400px", iframe:true});
});



/* ------------------ WHITE TRINANGLE IN DROP MENU ------------------ */
$("nav > ul > li").hover ( function () {
    var liWidth = $(this).innerWidth();
    $("nav span").css('width', liWidth);

    if ($.browser.msie) {
        if ( parseInt($.browser.version, 10) < 8 ) {
            $("nav ul li span").css('margin-left', -liWidth);
        }
    }
});