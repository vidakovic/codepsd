$(document).ready(function () {
    /****** ColorBox (lightbox photogalley and view source) ******/
    $(".look a").colorbox({
         fixed:true, iframe:true, transition: "none", height: "98%", width:"90%"
    });
    $(".portfolio a:has(img)").colorbox({
         fixed:true, transition: "none", height: "98%"
    });
    $(".portfolio .files a").colorbox({
         fixed:true, iframe:true, transition: "none", height: "98%", width:"90%"
    });
    $(".reference a:has(img)").colorbox({
         fixed:true, transition: "none", height: "98%"
    });


    var thisOffset=0;
    var thatOffset=0;
    //  highligh menu links in side bar on click
    $("aside li a").click(function () {
        prevOffset = $("aside li.active").offset();
        $("aside li").removeClass();
        $(this).parent().addClass("active");
        thisOffset =  $(this).parent().offset();
    });

    // smoothe scroll on menu clicks
    $('a[href*=#]').click(function() {
        if (location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                var offFromTop = -10;
                if(prevOffset.top > thisOffset.top) { offFromTop = 0; }
                var targetOffset = $target.offset().top-offFromTop;
                $('html,body').animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });
	
    // highligh menu links in side bar when the related article is scrolled into view
    $('article[id]').waypoint(function(event, direction) {
        $("aside li").removeClass();
        $('aside li a[href*="'+ $(this).attr('id') +'"]').parent().addClass("active");
    },{
        offset: -1
    });
    
  
  
    //  Hide logo if browser height is less then 540px. For small screens only!
    if (($(window).height()<547)) { // returns height of browser viewport
        $("#logo").css('display', 'none');
    }
    
    
    // set article margin-bottom relative to window height
    var bHeight = $(window).height()-500;
    $("article").not('article.no-margin').css('margin-bottom', bHeight);
    
	
	
    
    // NAROCI FORMA
    // Place ID's of all required fields here.
	required = ["email", "message"];
	// If using an ID other than #email or #error then replace it here
	email = $("#email");
	errornotice = $(".error");
	// The text to show up within a field when it is incorrect
	emptyerror = "Prosimo izpolnite to polje.";
	emailerror = "Prosimo vnesite pravi email naslov.";

	$("#upload").submit(function(){	
		//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
		// Validate the e-mail.
		if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email.val())) {
			email.addClass("needsfilled");
			email.val(emailerror);
		}

		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if ($(":input").hasClass("needsfilled")) {
			return false;
		} else {
			errornotice.hide();
            
			if ($('input[type=file]').val() != "") {
                $(".upload-img").show();
                $(".input-file").addClass("uploading");
                $(".input-file small").text("Pošiljam datoteke... Prosim počakajte. ");
            }
            
            // set value on submit button when email is being sent
            $("input:submit").attr('value', 'Pošiljam...' ).css({'background-color' : '#F75', 'background-image' : 'none', 'border-radius' : '2px', 'border' : '1px solid #888'});
            
			return true;
		}
	});
	
	// Clears any fields in the form when the user clicks on them
	$(":input").focus(function(){		
	   if ($(this).hasClass("needsfilled") ) {
			$(this).val("");
			$(this).removeClass("needsfilled");
	   }
	});
    
    
    //  show added files from input field
    if($.browser.msie) {  // if browser is IE
        $('input[type=file]').change(function(e){
            var $fileList = $("#fileList");
            $fileList.empty();
            $fileList.append('<li>PRILOŽENA DATOTEKA:</li>' );
            
            var $file = $('input[type=file]').val().split('\\').pop();;
            $fileList.append('<li>' + $file + '</li>' );
        });
    } else { // for all browsers except IE
        $('input:file').change(function() {
            var $fileList = $("#fileList");
            $fileList.empty();
            $fileList.append('<li>PRILOŽENE DATOTEKE:</li>' );
    
            for (var i = 0; i < this.files.length; i++) {
                var file = this.files[i];
                $fileList.append('<li>' + file.name + '</li>' );
            }
        });
    }
    
    // calculate and set value for price on load
    $('.total').text($('input:radio[name="days"]:checked').val()*7 + "€");
    
    // END NAROCI FORMA
    
    
    
    // ToolTips
    $('abbr').tooltip();
	
    // start google+1
    gapi.plusone.go();
});


   
// Kalkulacija cene
$(function() {

  var calculate = function() {
        var result = $('input:radio[name="days"]:checked').val();
        var pages = $('.pages').val();
        if(!isNaN(pages) && pages != 0) {
            if(pages == 1) { result = result * 7; }
            else { result = result*7 + result*(pages-1)*4; }
            $('.total').text(result + "€");
        }
        else {
            $('.pages').val(1);
            $('.total').text(result*7 + "€");
        }
  }

  $('.pages').keyup(calculate);
  $('input:radio').change(calculate);

});


$.fn.tooltip = function(){
    $(this).tooltips();
    $(this).tooltipMouseover();
    $(this).tooltipRemove();
}

// function for toolTips
$.fn.tooltips = function () {
    return this.each (function () {	
        $(this).data('title',$(this).attr('title'));
        $(this).removeAttr('title');
    });
}

// function for when abbreviations are mouseover-ed show a tooltip with the data from the title attribute
$.fn.tooltipMouseover = function () {
    return this.mouseover(function() {
        // first remove all existing abbreviation tooltips
        $(this).next('.tooltip').remove();
        // create the tooltip
        $(this).after('<span class="tooltip">' + $(this).data('title') + '</span>');
        // position the tooltip 4 pixels above and 4 pixels to the left of the abbreviation
        var left = $(this).position().left;
        var top = $(this).position().top + 28;
        $(this).next().css('left',left);
        $(this).next().css('top',top);
    });
}
// function for remove the tooltip on abbreviation mouseout
$.fn.tooltipRemove = function () {
    return this.mouseout(function() {
        $(this).next().animate({opacity:1},{duration:1, complete: function(){
            $(this).fadeOut(100);
        }});			
    });
};





// function for appears in browsers view
(function($,k,m,i,d){var e=$(i),g="waypoint.reached",b=function(o,n){o.element.trigger(g,n);if(o.options.triggerOnce){o.element[k]("destroy")}},h=function(p,o){var n=o.waypoints.length-1;while(n>=0&&o.waypoints[n].element[0]!==p[0]){n-=1}return n},f=[],l=function(n){$.extend(this,{element:$(n),oldScroll:0,waypoints:[],didScroll:false,didResize:false,doScroll:$.proxy(function(){var q=this.element.scrollTop(),p=q>this.oldScroll,s=this,r=$.grep(this.waypoints,function(u,t){return p?(u.offset>s.oldScroll&&u.offset<=q):(u.offset<=s.oldScroll&&u.offset>q)}),o=r.length;if(!this.oldScroll||!q){$[m]("refresh")}this.oldScroll=q;if(!o){return}if(!p){r.reverse()}$.each(r,function(u,t){if(t.options.continuous||u===o-1){b(t,[p?"down":"up"])}})},this)});$(n).scroll($.proxy(function(){if(!this.didScroll){this.didScroll=true;i.setTimeout($.proxy(function(){this.doScroll();this.didScroll=false},this),$[m].settings.scrollThrottle)}},this)).resize($.proxy(function(){if(!this.didResize){this.didResize=true;i.setTimeout($.proxy(function(){$[m]("refresh");this.didResize=false},this),$[m].settings.resizeThrottle)}},this));e.load($.proxy(function(){this.doScroll()},this))},j=function(n){var o=null;$.each(f,function(p,q){if(q.element[0]===n){o=q;return false}});return o},c={init:function(o,n){this.each(function(){var u=$.fn[k].defaults.context,q,t=$(this);if(n&&n.context){u=n.context}if(!$.isWindow(u)){u=t.closest(u)[0]}q=j(u);if(!q){q=new l(u);f.push(q)}var p=h(t,q),s=p<0?$.fn[k].defaults:q.waypoints[p].options,r=$.extend({},s,n);r.offset=r.offset==="bottom-in-view"?function(){var v=$.isWindow(u)?$[m]("viewportHeight"):$(u).height();return v-$(this).outerHeight()}:r.offset;if(p<0){q.waypoints.push({element:t,offset:null,options:r})}else{q.waypoints[p].options=r}if(o){t.bind(g,o)}if(n&&n.handler){t.bind(g,n.handler)}});$[m]("refresh");return this},remove:function(){return this.each(function(o,p){var n=$(p);$.each(f,function(r,s){var q=h(n,s);if(q>=0){s.waypoints.splice(q,1)}})})},destroy:function(){return this.unbind(g)[k]("remove")}},a={refresh:function(){$.each(f,function(r,s){var q=$.isWindow(s.element[0]),n=q?0:s.element.offset().top,p=q?$[m]("viewportHeight"):s.element.height(),o=q?0:s.element.scrollTop();$.each(s.waypoints,function(u,x){if(!x){return}var t=x.options.offset,w=x.offset;if(typeof x.options.offset==="function"){t=x.options.offset.apply(x.element)}else{if(typeof x.options.offset==="string"){var v=parseFloat(x.options.offset);t=x.options.offset.indexOf("%")?Math.ceil(p*(v/100)):v}}x.offset=x.element.offset().top-n+o-t;if(x.options.onlyOnScroll){return}if(w!==null&&s.oldScroll>w&&s.oldScroll<=x.offset){b(x,["up"])}else{if(w!==null&&s.oldScroll<w&&s.oldScroll>=x.offset){b(x,["down"])}else{if(!w&&o>x.offset){b(x,["down"])}}}});s.waypoints.sort(function(u,t){return u.offset-t.offset})})},viewportHeight:function(){return(i.innerHeight?i.innerHeight:e.height())},aggregate:function(){var n=$();$.each(f,function(o,p){$.each(p.waypoints,function(q,r){n=n.add(r.element)})});return n}};$.fn[k]=function(n){if(c[n]){return c[n].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof n==="function"||!n){return c.init.apply(this,arguments)}else{if(typeof n==="object"){return c.init.apply(this,[null,n])}else{$.error("Method "+n+" does not exist on jQuery "+k)}}}};$.fn[k].defaults={continuous:true,offset:0,triggerOnce:false,context:i};$[m]=function(n){if(a[n]){return a[n].apply(this)}else{return a.aggregate()}};$[m].settings={resizeThrottle:200,scrollThrottle:100};e.load(function(){$[m]("refresh")})})(jQuery,"waypoint","waypoints",this);
//(function($,k,m,i,d){var e=$(i),g="waypoint.reached",b=function(o,n){o.element.trigger(g,n);if(o.options.triggerOnce){o.element[k]("destroy")}},h=function(p,o){var n=o.waypoints.length-1;while(n>=0&&o.waypoints[n].element[0]!==p[0]){n-=1}return n},f=[],l=function(n){$.extend(this,{element:$(n),oldScroll:-99999,waypoints:[],didScroll:false,didResize:false,doScroll:$.proxy(function(){var q=this.element.scrollTop(),p=q>this.oldScroll,s=this,r=$.grep(this.waypoints,function(u,t){return p?(u.offset>s.oldScroll&&u.offset<=q):(u.offset<=s.oldScroll&&u.offset>q)}),o=r.length;if(!this.oldScroll||!q){$[m]("refresh")}this.oldScroll=q;if(!o){return}if(!p){r.reverse()}$.each(r,function(u,t){if(t.options.continuous||u===o-1){b(t,[p?"down":"up"])}})},this)});$(n).scroll($.proxy(function(){if(!this.didScroll){this.didScroll=true;i.setTimeout($.proxy(function(){this.doScroll();this.didScroll=false},this),$[m].settings.scrollThrottle)}},this)).resize($.proxy(function(){if(!this.didResize){this.didResize=true;i.setTimeout($.proxy(function(){$[m]("refresh");this.didResize=false},this),$[m].settings.resizeThrottle)}},this));e.load($.proxy(function(){this.doScroll()},this))},j=function(n){var o=null;$.each(f,function(p,q){if(q.element[0]===n){o=q;return false}});return o},c={init:function(o,n){this.each(function(){var u=$.fn[k].defaults.context,q,t=$(this);if(n&&n.context){u=n.context}if(!$.isWindow(u)){u=t.closest(u)[0]}q=j(u);if(!q){q=new l(u);f.push(q)}var p=h(t,q),s=p<0?$.fn[k].defaults:q.waypoints[p].options,r=$.extend({},s,n);r.offset=r.offset==="bottom-in-view"?function(){var v=$.isWindow(u)?$[m]("viewportHeight"):$(u).height();return v-$(this).outerHeight()}:r.offset;if(p<0){q.waypoints.push({element:t,offset:t.offset().top,options:r})}else{q.waypoints[p].options=r}if(o){t.bind(g,o)}});$[m]("refresh");return this},remove:function(){return this.each(function(o,p){var n=$(p);$.each(f,function(r,s){var q=h(n,s);if(q>=0){s.waypoints.splice(q,1)}})})},destroy:function(){return this.unbind(g)[k]("remove")}},a={refresh:function(){$.each(f,function(r,s){var q=$.isWindow(s.element[0]),n=q?0:s.element.offset().top,p=q?$[m]("viewportHeight"):s.element.height(),o=q?0:s.element.scrollTop();$.each(s.waypoints,function(u,x){var t=x.options.offset,w=x.offset;if(typeof x.options.offset==="function"){t=x.options.offset.apply(x.element)}else{if(typeof x.options.offset==="string"){var v=parseFloat(x.options.offset);t=x.options.offset.indexOf("%")?Math.ceil(p*(v/100)):v}}x.offset=x.element.offset().top-n+o-t;if(s.oldScroll>w&&s.oldScroll<=x.offset){b(x,["up"])}else{if(s.oldScroll<w&&s.oldScroll>=x.offset){b(x,["down"])}}});s.waypoints.sort(function(u,t){return u.offset-t.offset})})},viewportHeight:function(){return(i.innerHeight?i.innerHeight:e.height())},aggregate:function(){var n=$();$.each(f,function(o,p){$.each(p.waypoints,function(q,r){n=n.add(r.element)})});return n}};$.fn[k]=function(n){if(c[n]){return c[n].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof n==="function"||!n){return c.init.apply(this,arguments)}else{if(typeof n==="object"){return c.init.apply(this,[null,n])}else{$.error("Method "+n+" does not exist on jQuery "+k)}}}};$.fn[k].defaults={continuous:true,offset:0,triggerOnce:false,context:i};$[m]=function(n){if(a[n]){return a[n].apply(this)}else{return a.aggregate()}};$[m].settings={resizeThrottle:200,scrollThrottle:100};e.load(function(){$[m]("refresh")})})(jQuery,"waypoint","waypoints",this);

