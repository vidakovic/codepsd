<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Reference - PSD razrez</title>
    <meta name="description" content="Naše reference - naši končni izdelki" />
    <meta name="keywords" content="psd2html, email template, email, magento, magento theme" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main" class="reference">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article>
                <h1>Naše <span>reference</span></h1>
                
                <p>
                    Dolgo časa smo čakali in mislili, da moramo počakati na velike naročnike in bomo šele nato objavili reference.
                    Sedaj pa smo spoznali, da takšnih najverjetneje ne bomo nikoli zmožni pokazati. Naši naročniki so spletne agencije,
                    ki nam zaradi pogodbe o razkritju informacij ne dovolijo razglasiti našega sodelovanja z njimi.
                    Ravno prek njih pa dobimo tiste dobre reference, kot sta recimo podjetji Dana, Pipistrel, Tilia ipd.
                    Te reference niso naše, ampak so podane kot primer, ker imamo kar nekaj podobnih.
                    Naši naročniki so tudi večja podjetja, ki outsourcajo in tega ne želijo razglašati.
                    Oni nam prav tako zaradi zgoraj omenjene pogodbe ne dovolijo pokazati referenc. Ostanejo nam torej samo še manjša podjetja in s. p.-ji.
                </p>
                <br />
                <p class="last">
                    Upamo, da razumete naš položaj. Pomislite, kaj je za vas res pomembno – imena naših referenc ali kako dobri so naši izdelki.
                    Nekaj primerov kode slednjih si lahko ogledate na strani <?php echo anchor('portfolio','Primeri izdelkov'); ?>.
                    Zadovoljili smo že mnogo naročnikov, zato verjamemo, da lahko tudi vas.
                    Edini način, da boste lahko zares prepričani v našo kakovost, pa je, da naredite tako, kot so vsi ostali. Torej, da nas preizkusite.
                </p>
                
                <figure>
                    <a href="/img/references/clovereasyweb.jpg">
                        <img src="/img/references/clovereasyweb_thumb.jpg" alt="Clovereasyweb" height="112" width="190" />
                    </a>
                    <p>
                        Clovereasyweb<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/coprnije2.jpg">
                        <img src="/img/references/coprnije2_thumb.jpg" alt="Coprnije2" height="112" width="190" />
                    </a>
                    <p>
                        Crystal-intuitive<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/eurora.jpg">
                        <img src="/img/references/eurora_thumb.jpg" alt="Eurora.si" height="112" width="190" />
                    </a>
                    <p>
                        Eurora.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/feri_mb_fb.jpg">
                        <img src="/img/references/feri_mb_fb_thumb.jpg" alt="Elektro fakulteta Maribor" height="140" width="149" />
                    </a>
                    <p>
                        Elektro fakulteta Maribor<br />
                        (psd2html for facebook app)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/freestylernetwork_email.jpg">
                        <img src="/img/references/freestylernetwork_email_thumb.jpg" alt="Freestylernetwork.com Email" height="140" width="135" />
                    </a>
                    <p>
                        Freestylernetwork.com<br />
                        (email template)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/humanresponse.jpg">
                        <img src="/img/references/humanresponse_thumb.jpg" alt="Humanresponse.ca" height="112" width="190" />
                    </a>
                    <p>
                        Humanresponse.ca<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/coprnije1.jpg">
                        <img src="/img/references/coprnije1_thumb.jpg" alt="Coprnije1" height="140" width="131" />
                    </a>
                    <p>
                        Intuicija<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/coprnije1_email1.jpg">
                        <img src="/img/references/coprnije1_email1_thumb.jpg" alt="Coprnije1 Email1" height="140" width="101" />
                    </a>
                    <p>
                        Intuicija<br />
                        (email template)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/coprnije1_email2.jpg">
                        <img src="/img/references/coprnije1_email2_thumb.jpg" alt="Coprnije1 Email2" height="140" width="133" />
                    </a>
                    <p>
                        Intuicija<br />
                        (email template)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/najdiljubezen.jpg">
                        <img src="/img/references/najdiljubezen_thumb.jpg" alt="Najdiljubezen.si" height="140" width="51" />
                    </a>
                    <p>
                        Najdiljubezen.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/odbitacena.jpg">
                        <img src="/img/references/odbitacena_thumb.jpg" alt="Piroplanet" height="140" width="86" />
                    </a>
                    <p> 
                        Odbitacena.si<br />
                        (Magento theme)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/piroplanet.jpg">
                        <img src="/img/references/piroplanet_thumb.jpg" alt="Piroplanet" height="140" width="131" />
                    </a>
                    <p>
                        Piroplanet.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/polceneje.jpg">
                        <img src="/img/references/polceneje_thumb.jpg" alt="polceneje" height="140" width="140" />
                    </a>
                    <p>
                        Polceneje.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/pressclip.jpg">
                        <img src="/img/references/pressclip_thumb.jpg" alt="Pressclip" height="140" width="150" />
                    </a>
                    <p>
                        Pressclip.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/sasoavsenik.jpg">
                        <img src="/img/references/sasoavsenik_thumb.jpg" alt="Ansambel Saša Avsenika" height="131" width="190" />
                    </a>
                    <p>
                        Ansambel Saša Avsenika<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/sugarcreative.jpg">
                        <img src="/img/references/sugarcreative_thumb.jpg" alt="Sugarcreative.dk" height="100" width="190" />
                    </a>
                    <p>
                        Sugarcreative.dk<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/tournament.jpg">
                        <img src="/img/references/tournament_thumb.jpg" alt="Tournament.si" height="140" width="167" />
                    </a>
                    <p>
                        Tournament.si<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/volilnipulz2011.jpg">
                        <img src="/img/references/volilnipulz2011_thumb.jpg" alt="volilnipulz2011" height="140" width="67" />
                    </a>
                    <p>
                        Volilni pulz 2011<br />
                        (psd2html)
                    </p>
                </figure>
                
                <figure>
                    <a href="/img/references/coprnije3.jpg">
                        <img src="/img/references/coprnije3_thumb.jpg" alt="Coprnije3" height="131" width="190" />
                    </a>
                    <p>
                        Žalovanje<br />
                        (psd2html)
                    </p>
                </figure>
            </article>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>