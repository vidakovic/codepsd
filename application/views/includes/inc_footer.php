    <footer>
    	<div class="fade">    <!-- fading effect at the bottom -->
            <p>
                Copyright © CodePsd.si
                <?php echo anchor('kontakt','Kontakt'); ?>

                <span> | </span>
                <?php echo anchor('o-nas','O nas'); ?>

                <span> | </span>
                <?php echo anchor('razrez-psd','Razrez PSD'); ?>

            </p>
        </div>
    </footer>
</div>    <!--! end of #wraper -->

    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/libs/psd_css.js">\x3C/script>')</script>

    <!-- plugins -->
    <script src="/js/plugins/psd_html.js"></script>
    
    <!-- scripts -->
    <script src="/js/razrez_psd.js"></script>
    
    <!-- Google analytics -->
    <script>
        var _gaq=[["_setAccount","UA-4350831-5"],["_trackPageview"]];
        (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
        g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
        s.parentNode.insertBefore(g,s)}(document,"script"));
    </script>

</body>
</html>