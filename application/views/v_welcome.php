<?php $this->load->view('/includes/inc_head_top.php');?>
    <title>CodePsd - Razrez PSD, razrez spletnih predlog(PSD v HTML)</title>
    <meta name="description" content="Delamo razrez PSD datotek v valid HTML, valid CSS in JavaScript kodo (jQuery ...)." />
    <meta name="keywords" content="razrez psd, psd v html, razrez spletnih predlog, psd2html, front-end, design, dizajn" />
<?php $this->load->view('/includes/inc_head_btm.php');?>
<?php $this->load->view('/includes/inc_header_top.php');?>
<?php $this->load->view('/includes/inc_navigation.php');?>
<?php $this->load->view('/includes/inc_header_btm.php');?>


    <div id="main" class="front">    <!-- main content and sidebar area -->
<?php $this->load->view('/includes/inc_logo.php');?>

            <nav>
                <ul>    <!-- side navigation -->
                    <li class="active"><?php echo anchor('#top','Na vrh'); ?></li>
                    <li><?php echo anchor('#our-work','Kaj delamo'); ?></li>
                    <li><?php echo anchor('#you-get','Kaj dobite'); ?></li>
                    <li><?php echo anchor('#order','Potek naročila'); ?></li>
                </ul>    <!-- end navigation-->
            </nav>
<?php $this->load->view('/includes/inc_contact.php');?>


        <div id="content">    <!-- content -->
            <article id="top">
                <h1>Potrebujete kakovosten <span>razrez PSD</span> v HTML?</h1>
                
                <div class="look">
                    <h3>Poglejte naš <a href="/files/codepsd.si/index.php" rel="nofollow">HTML</a></h3>
                    <a class="window" href="/files/codepsd.si/index.php" rel="nofollow"></a>
                </div>

                <div class="look right">
                    <h3>Poglejte naš <a href="/files/codepsd.si/css.php" rel="nofollow">CSS</a></h3>
                    <a class="window" href="/files/codepsd.si/css.php" rel="nofollow"></a>
                </div>
            </article>
            
            <article id="our-work">
                <h2><span>S čim</span> se ukvarjamo?</h2>
                
                <strong>S ČIM TOČNO SE UKVARJAMO?</strong>
                <p>
                    Delamo <?php echo anchor('razrez-psd','razrez PSD'); ?> (PSD2HTML) ali z drugimi besedami ročno kodiramo vaše spletne predloge v validiran in semantično urejen
                    <abbr title="Hyper Text Markup Language">HTML</abbr> in <abbr title="Cascading Style Sheets">CSS</abbr>.
                    Optimiziramo kodo, datoteke in slike za gladko in hitro nalaganje strani. Kodo tudi dokumentiramo, da vam v prihodnje omogočimo preprosto
                    urejanje in modifikacijo. Delamo po najnovejših standardih. Slike npr. lahko optimiziramo s tehniko
                    <a href="http://www.w3schools.com/css/css_image_sprites.asp">CSS sprites</a>, kar zmanjša število zahtevkov
                    <abbr title="Hypertext Transfer Protocol">HTTP</abbr>.To pa še občutno pospeši nalaganje strani.
                </p>
                <strong>KAJ DOBITE VI?</strong>
                <p>
                    Vi dobite validirano in semantično urejeno kodo HTML in CSS iz vašega designa. Ta vaši strani omogoča kompatibilnost v vseh trenutnih brskalnikih –
                    torej Google Chrome, Mozilla Firefox 3.6+, Opera 11+, Safari 5+ in Internet Explorer 7+.
                </p>
                <strong>KAKO NAM LAHKO POŠLJETE DATOTEKE?</strong> 
                <p>
                    Pošljite nam jih prek našega <?php echo anchor('naroci','naročilnega'); ?> obrazca ali na naš
                    elektronski naslov <a href="mailto:info@codepsd.si">info@codepsd.si</a>.
                </p>
                <strong>KAJ ŠE LAHKO NAREDIMO ZA VAS?</strong> 
                <p>
                    Delamo še veliko več kot samo front-end razrez datotek .psd v HTML in CSS (<?php echo anchor('razrez-psd','razrez PSD'); ?>).
                    Pretvarjamo vse slikovne formate v HTML, kodiramo elektronsko pošto (newsletter) v HTML, implementiramo <abbr title="Skriptni Programski Jezik">JavaScript</abbr>
                    in <abbr title="JavaScript knjižnica">jQuery</abbr> itd. Nudimo tudi razrez in implementacijo dizajnov v zelo priljubljen
                    <abbr title="Content Managemnet System">CMS</abbr> kot je <a href="http://drupal.org/">Drupal</a>.
                </p>
                <strong>ZA KOGA SO NAŠE STORITVE?</strong>
                <p>* za podjetja, ki izdelujejo spletne strani</p>
                <p>* za spletne agencije, ki potrebujejo zasebnost</p>
                <p>* za spletne razvijalce, ki vedo, da je njihov čas prepomemben za razrez</p>
                <p>* za lastnike spletnih strani, ki potrebujejo prenovo svoje strani</p>
                <p>* za ljudi, ki imajo neskodiran dizajn in potrebujejo <?php echo anchor('razrez-psd','razrez PSD'); ?> (PSD2HTML)</p>
                <p>* za vse ostale...</p>
                <strong>ZAKAJ MI?</strong>
                <p>* Ker smo postali pravi strokovnjaki za front-end razrez HTML/CSS in to res obvladamo!</p>
                <p>* Ker imamo že večletne izkušnje in redno spremljamo in se učimo nove tehnologije in tehnike!</p>
                <p>* Ker se vedno držimo dogovorov in rokov!</p>
                <p>* Ker smo pošteni, hitri, a natančni!</p>
                <p>* Ker znamo zasebnost obdržati zasebno!</p>
                <p>* In še mnogo drugega...</p>
            </article>
            
            <article id="you-get">
                <h2><span>Kaj dobite</span> vi?</h2>
                
                <div class="left box">
                    <strong><abbr title="World Wide Web Consortium">W3C</abbr> VALIDIRANO KODO</strong>
                    <p>Ponujamo vam W3C skladne PSD v HTML pretvorbne storitve. Naša ekipa bo spremenila vaše designe v do piksla natačno podobo za brskalnik.</p>
                </div>
                <div class="box">
                    <strong>KOMPATIBILNOST</strong>
                    <p>
                        Naša koda vaši strani omogoča kompatibilnost v vseh trenutnih brskalnikih
                        (IE7, IE8, IE9, Chrome, Firefox 3.5+, Safari 4+ in Opera 10+).
                    </p>
                </div>
                <div class="box">
                    <strong>OPTIMIZACIJA</strong>
                    <p>
                        Delamo z najnovejšimi standardi. Vaše datoteke optimiziramo tako, da stisnemo njihovo velikost.
                        Zmanjšamo tudi število zahtevkov HTTP, to pa še bolj pospeši nalaganje strani.
                    </p>
                </div>
                <div class="left box">
                    <h3><abbr title="Search Engine Optimization">SEO</abbr> IN ČISTA KODA</h3> 
                    <p>
                        Rezultat našega dela je validiran HTML-dokument skupaj s CSS in JavaScript.
                        Vsa koda je semantično urejena in optimizirana za iskalnike (SEO).
                    </p>
                </div>
                <div class="box">
                    <h3><abbr title="Skriptni Programski Jezik">JAVASCRIPT</abbr> KNJIžNICE</h3> 
                    <p>
                        Za vas lahko implementiramo JavaScript. Ne samo standarden JavaScript,
                        ampak tudi različne JavaScript knjižnice, kot recimo trenutno popularno jQuery.
                    </p>
                </div>
                <div class="box">
                    <strong>HITRA DOSTAVA</strong>
                    <p>
                        Potrebujete svoje datoteke hitro? Ni problema! Pošljite nam jih prek našega 
                        <?php echo anchor('naroci','naročilnega'); ?> obrazca ali na naš elektronski naslov <a href="mailto:info@codepsd.si">info@codepsd.si</a>.
                    </p>
                </div>
            </article>

            <article id="order">
                <h2>Kako poteka <span>naročilo</span>?</h2>
                
                <div class="left box">
                    <strong>KORAK 1</strong>
                    <p>
                        Pošljite nam dizajne prek našega <?php echo anchor('naroci','obrazca'); ?> ali na naš elektronski naslov.
                        Zraven podajte čimbolj natančna navodila, da vam lahko pošlejmo točno oceno potrebnega časa za izvedbo.
                    </p>
                </div>
                <div class="box">
                    <strong>KORAK 2</strong>
                    <p>
                        Po prejetju naše ocene vi izvedbo samo potrdite, mi pa bomo takoj po prejemu potrdila začeli z delom.
                    </p>
                </div>
                <div class="box">
                    <strong>KORAK 3</strong> 
                    <p>
                        Končan izdelek vam pošljemo zazipan na vaš elektronski naslov. Lahko pa datoteke naložimo na naš strežnik,
                        da preverite, ali slučajno želite še kakšne spremembe.
                    </p>
                </div>

                <strong>&nbsp;Že od 16 € na uro dalje. <?php echo anchor('pogosta-vprasanja#quote','Primer'); ?> ocene</strong>
            </article>
        </div>    <!-- end content -->
    </div>    <!--! end of main content and sidebar -->


<?php $this->load->view('/includes/inc_footer.php');?>