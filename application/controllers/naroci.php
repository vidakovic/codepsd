<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Naroci extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
        $this->load->library(array('form_validation', 'email'));
		$this->load->helper(array('form', 'url'));
	}
   
	function index()
	{
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('message', 'Sporočilo', 'trim|required|min_length[2]|max_length[9999]');

		$this->form_validation->set_rules('html', 'HTML', '');
		$this->form_validation->set_rules('css', 'CSS', '');
		$this->form_validation->set_rules('days', 'DAYS', '');
		$this->form_validation->set_rules('userfile', 'Datoteka', '');
        
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');

        if ($this->form_validation->run())
        {
            $radio1 = (set_value('html')) ? "<br /> Naročnik želi: ".set_value('html') : "";
            $radio2 = (set_value('css')) ? "<br /> Naročnik želi: ".set_value('css') : "";
            $radio3 = (set_value('days')) ? "<br /> Naročnik potrebuje razrez po: ".set_value('days')."€" : "";
    
        	$this->email->from(set_value('email'));
            $this->email->to('info@codepsd.si');

        	$this->email->subject("codepsd.si->NAROČILO");
        	$this->email->message(set_value('message')."<br /><br />". $radio1 . $radio2 . $radio3);
            
            
        	$config['upload_path'] = 'uploads/';
        	$config['allowed_types'] = 'zip|rar|7z|gif|jpg|png|psd|bmp|ttf|eot';
        	$config['max_size']	= '25600';
        
        	$this->load->library('upload', $config);

            // upload files
            $nrFiles = count($_FILES['file']['name']);
            $upFiles = array();
            if(!empty($_FILES['file']['name'][0]))
            {
                $upload = true;
                foreach($_FILES['file'] as $key => $value)
                {
                    $i = 0;
                    foreach ($value as $item) 
                    {
                        $data[$i][$key] = $item;
                        $i++;
                    }
                }
                $_FILES = $data; // re-declarate
                for($i=0; $i<$nrFiles; $i++)
                {
                    if($this->upload->do_upload($i))
                    {
                        $file = $this->upload->data();
                        array_push($upFiles, $file['file_name']);
                    }
                    else
                    {
                        $data['message'] = $this->upload->display_errors();
                        $this->load->view('v_naroci', $data);
                        return;
                    }
                } 
            }
            if($upFiles)
            {
                $file_list = "";
                foreach($upFiles as $value)
                {
                    $file_list .= base_url() . "uploads/" . $value . "<br />";
                }
                $this->email->message(set_value('message')."<br /><br />". $radio1 . $radio2 . $radio3 ."<br /><br />Priložene datoteke:<br />" . $file_list);
            }
            

            // send email
            if($this->email->send())
            {
                /*
                if($upFiles)
                {
                    foreach($upFiles as $value)
                    {
                        unlink('uploads/'.$value);
                    }
                }
                */
                $data['message'] = '<span class="success">Vaše sporočilo je bilo uspešno poslano!</span>';
                $this->load->view('v_naroci', $data);
            }
            else
            {
                //show_error($this->email->print_debugger());
                $data['message'] = '<span class="success error">"Prišlo je do napake! Vaše sporočilo ni bilo poslano. Pošljite nam mail na <a href="mailto:info@codepsd.si">info@codepsd.si</a></span>';
                $this->load->view('v_naroci', $data);
            }
        }

		else
		{
            $data['message'] = '';
            $this->load->view('v_naroci', $data);
		}
	}
}
            